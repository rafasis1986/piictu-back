# -*- coding: utf-8 -*-
'''
Created on 24/02/2015

@author: rtorres
'''
import sys

import requests

import json


def crear_usuario(nombre_usuario, correo, clave):
    parametros = {'username': nombre_usuario, 'email': correo, 'password': clave}
    r = requests.post('http://localhost:8000/user/create/',
                      data=parametros, verify=False)
    return json.loads(r.content)


def consulta_token(nombre_usuario, clave):
    parametros = {'username': nombre_usuario, 'password': clave}
    r = requests.post('http://127.0.0.1:8000/api-token-auth/',
                      data=parametros, verify=False)
    return json.loads(r.content)


def consultar_reddits(nombre, token):
    parametros = {'name': nombre}
    headers = {'Authorization': 'JWT ' + token}
    r = requests.post('http://127.0.0.1:8000/reddits/',
                      data=parametros, headers=headers, verify=False)
    return json.loads(r.content)


def afiliar_reddits(token):
    names = ['art', 'developer', 'music', 'python']
    print 'lista de reddits para afiliar: ', names
    parametros = {'subreddits_list': json.dumps(names)}
    headers = {'Authorization': 'JWT ' + token}
    r = requests.post('http://127.0.0.1:8000/reddits/affiliate/',
                      data=parametros, headers=headers, verify=False)
    return json.loads(r.content)


def desafiliar_reddits(token):
    names = ['art', 'developer', 'music', 'python']
    print 'lista de reddits para desafiliar: ', names
    parametros = {'subreddits_list': json.dumps(names)}
    headers = {'Authorization': 'JWT ' + token}
    r = requests.post('http://127.0.0.1:8000/reddits/disaffiliate/',
                      data=parametros, headers=headers, verify=False)
    return json.loads(r.content)

def obtener_submissions(token, name_reddit, cant):
    parametros = {'name': name_reddit, 'limit_posts': cant}
    headers = {'Authorization': 'JWT ' + token}
    r = requests.post('http://127.0.0.1:8000/reddits/news/',
                      data=parametros, headers=headers, verify=False)
    return json.loads(r.content)

def main(argv):
    username = argv[0]
    password = argv[1]
    email = argv[2]
    funcion = argv[3].upper()
    if funcion == 'C':
        print crear_usuario(username, email, password)
    elif funcion == 'T':
        print consulta_token(username, password)
    elif funcion == 'R':
        token = consulta_token(username, password)
        print consultar_reddits(argv[4], token['token'])
    elif funcion == 'A':
        token = consulta_token(username, password)
        print afiliar_reddits(token['token'])
    elif funcion == 'D':
        token = consulta_token(username, password)
        print desafiliar_reddits(token['token'])
    elif funcion == 'S':
        token = consulta_token(username, password)
        print obtener_submissions(token['token'], argv[4], int(argv[5]))
    else:
        print "Accion Invalida \n"
        print "Ingrese #username #password #email junto a alguna de las siguientes opciones: \n"
        print "- Afiliar reddits a su cuenta: A"
        print "- afiliar su Cuenta a un usuario:  C"
        print "- Desafiliar reddits de su cuenta: D"
        print "- consultar similitudes de un Reddit: R #nombre_reddit"
        print "- obtener Submissions de un reddit: S #nombre_reddit #cantidad_submissions"
        print "- consultar Token del usuario: T "


if __name__ == "__main__":
    if len(sys.argv) > 3 :
        main(sys.argv[1:])
    else:
        print "faltan argumentos"
