# -*- coding: utf-8 -*-
'''
Created on 21/2/2015

@author: rtorres
'''
def configLocal():
    config = {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'piictu-back',
        'USER': 'piictu',
        'PASSWORD': 'qwerty',
        'HOST': 'localhost',
        'PORT': '5432',
        }
    return config
