from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

from reddit.views import get_similar_subreddits, create_user, affiliate_subreddits, \
    disaffiliate_subreddits, get_new_posts, SubRedditList, get_subreddits


# Uncomment the next two lines to enable the admin:
admin.autodiscover()

urlpatterns = patterns('',
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls')),
    # url(r'^api-token-auth/', views.obtain_auth_token),
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^reddits/$', get_similar_subreddits),
    url(r'^reddits/affiliate/$', affiliate_subreddits),
    url(r'^reddits/disaffiliate/$', disaffiliate_subreddits),
    url(r'^reddits/news/$', get_new_posts),
    url(r'^subreddits/list/', SubRedditList.as_view()),
    url(r'^user/create/$', create_user),
    url(r'^subs/lists/$', get_subreddits),
)

# Uncomment the next line to serve media files in dev.
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
                            url(r'^__debug__/', include(debug_toolbar.urls)),
                            )
