# -*- coding: utf-8 -*-
'''
Created on 23/02/2015

@author: rtorres
'''
DB_APP = 'reddit'

MAX_SUBREDDITS = 10

REDDIT_GROUP = 'REDDIT'

USER_AGENT = 'PRAW_test_suite'
