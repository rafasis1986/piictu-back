# -*- coding: utf-8 -*-
from django.contrib.auth.models import User, Group
from django.db import models
from django.db.transaction import atomic

import json
from reddit.constantes import REDDIT_GROUP, DB_APP


class SubRedditModel(models.Model):
    id = models.CharField(max_length=32, primary_key=True)
    fullname = models.CharField(max_length=32, db_index=True)
    display_name = models.CharField(max_length=128, unique=True)
    title = models.CharField(max_length=256)
    subscribers = models.PositiveIntegerField()
    url = models.URLField()

    class Meta:
        db_table = DB_APP + '_subreddit'

    def save(self, *args, **kwargs):
        self.title = self.title.strip()
        self.display_name = self.display_name.strip().lower()
        return super(SubRedditModel, self).save(*args, **kwargs)


class UserRedditModel(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    subreddits = models.ManyToManyField(SubRedditModel)
    reddit_key = models.CharField(max_length=128)

    class Meta:
        db_table = DB_APP + '_user'

    @atomic
    def save(self, *args, **kwargs):
        self.user.groups.add(Group.objects.get(name=REDDIT_GROUP))
        return super(UserRedditModel, self).save(*args, **kwargs)


class PostModel(object):

    def __init__(self):
        self.id = 0
        self.title = ''
        self.score = 0
        self.date_created = None
        self.link = ''

    def to_JSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
            sort_keys=True, indent=4)
