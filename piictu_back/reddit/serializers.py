# -*- coding: utf-8 -*-
'''
Created on 21/2/2015

@author: rtorres
'''
from django.contrib.auth.models import User, Group
from rest_framework import serializers

from reddit.models import SubRedditModel, PostModel


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Group
        fields = ('url', 'name')


class SubRedditSerializer(serializers.ModelSerializer):

    class Meta:
        model = SubRedditModel
        fields = ('id', 'fullname', 'display_name', 'title', 'subscribers', 'url',)


class PostModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = PostModel
        fields = ('id', 'title', 'score', 'date_created', 'link',)
