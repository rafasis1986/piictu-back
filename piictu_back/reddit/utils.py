# -*- coding: utf-8 -*-
'''
Created on 24/02/2015

@author: rtorres
'''
from django.contrib.auth.models import User


def exist_user(username):
    '''
    @param username: user login
    @type  username: string

    @return: True in case that the user exists
    '''
    try:
        User.objects.get(username=username)
    except User.DoesNotExist:
        return False
    return True


def find_subredits(reddit, name_subreddit):
    list_subs = []
    subs = reddit.search_reddit_names(query=name_subreddit)
    for item in subs:
        list_subs.append({'display_name': str(item.display_name), 'subscribers': item.subscribers})
    return list_subs


def json_default(o):
    return o.__dict__
