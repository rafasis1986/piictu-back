# -*- coding: utf-8 -*-

import datetime

from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.http.response import HttpResponse
import praw
from praw.errors import InvalidUserPass
from rest_framework import viewsets, permissions, generics
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

import base64
import json
from reddit.constantes import USER_AGENT, MAX_SUBREDDITS
from reddit.error_messages import DONT_EXIST_REDDIT
from reddit.models import UserRedditModel, SubRedditModel, PostModel
from reddit.serializers import UserSerializer, GroupSerializer, SubRedditSerializer
from reddit.utils import exist_user, find_subredits
from rest_framework.status import HTTP_400_BAD_REQUEST


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.AllowAny]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class SubRedditList(generics.ListAPIView):
    queryset = SubRedditModel.objects.all()
    serializer_class = SubRedditSerializer
    permission_classes = [permissions.AllowAny]


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def get_subreddits(request):
    userApp = UserRedditModel.objects.get(user=request.user)
    serials = []
    for item in userApp.subreddits.all():
        serial = SubRedditSerializer(item)
        serials.append(serial.data)
    return JSONResponse(serials)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def get_similar_subreddits(request):
    '''
    @param name: name of subreddit to search
    @type  name: string

    @return: list of similar subreddits
    '''
    list_subs = []
    try:
        name = request.DATA.get('name')
        reddit = praw.Reddit(USER_AGENT, disable_update_check=True)
        list_subs = find_subredits(reddit, name)
    except KeyError:
        error = {"error": "falta name"}
        return Response(error)
    except IOError:
        error = {"error": DONT_EXIST_REDDIT}
        return Response(error)
    return Response(json.dumps(list_subs))


@api_view(['POST', ])
@transaction.atomic
@permission_classes((AllowAny,))
def create_user(request):
    '''
    @return: response: true if user has been created
    '''
    user = None
    answer = {}
    if request.method == 'POST':
        try:
            username = request.DATA.get('username')
            email = request.DATA.get('email')
            password = request.DATA.get('password')
            user = None
            if exist_user(username):
                answer = {"code": 400, "message": "The user is already registered in the application", "error" : True}
            else:
                reddit = praw.Reddit(USER_AGENT, disable_update_check=True)
                reddit.login(username, password)
                user = User.objects.create_user(username, email, password)
                user.is_active = True
                user.save()
                reddit_user = UserRedditModel()
                reddit_user.user = user
                reddit_user.reddit_key = base64.encodestring(password)
                reddit_user.save()
        except InvalidUserPass:
            answer = {"code": 400, "message": "Reddit user account is incorrect", "error" : True}
        except Exception:
            answer = {"code": 400, "message": "General Error", "error" : True}
        except:
            answer = {"code": 400, "message": "Error, please contact to administrator", "error" : True}
        if answer.get("error", False):
            return Response(answer, status=HTTP_400_BAD_REQUEST)
        return Response(answer)


@api_view(['POST'])
@transaction.atomic
@permission_classes((IsAuthenticated,))
def affiliate_subreddits(request):
    answer = {}
    if request.method == 'POST':
        try:
            reddit_user = UserRedditModel.objects.get(user=request.user)
            reddit = praw.Reddit(USER_AGENT, disable_update_check=True)
            reddit.login(reddit_user.user.username, base64.decodestring(reddit_user.reddit_key))
            candidates_list = request.DATA.get('subreddits_list')
            answer_list = []
            if len(candidates_list) > MAX_SUBREDDITS:
                raise Exception("The user has many subreddits")
            if reddit_user.subreddits.count() < MAX_SUBREDDITS:
                subr_dict = dict()
                for subr_name in candidates_list:
                    subr_name = subr_name.strip().lower()
                    subr_dict['name'] = subr_name
                    if (reddit_user.subreddits.filter(display_name=subr_name)):
                        subr_dict['success'] = False
                        subr_dict['error'] = "It is already associated with the user"
                    elif reddit_user.subreddits.count() < MAX_SUBREDDITS:
                        try:
                            reddit.get_subreddit(subr_name).subscribe()
                            subreddit = reddit.get_subreddit(subr_name)
                            try:
                                sub_model = SubRedditModel.objects.get(id=subreddit.id)
                            except ObjectDoesNotExist:
                                sub_model = SubRedditModel()
                                sub_model.display_name = subreddit.display_name
                                sub_model.id = subreddit.id
                                sub_model.subscribers = subreddit.subscribers
                                sub_model.title = subreddit.title
                                sub_model.fullname = subreddit.fullname
                                sub_model.url = subreddit.url
                                sub_model.save()
                            finally:
                                reddit_user.subreddits.add(sub_model)
                                subr_dict['success'] = True
                        except:
                            subr_dict['success'] = False
                            subr_dict['error'] = "Problems consuling the subreddit: %s" % subr_name
                    else:
                        subr_dict['success'] = False
                        subr_dict['error'] = "Reddit account already has %s subreddits" % MAX_SUBREDDITS
                    answer_list.append(subr_dict)
                answer['subreddits_list'] = answer_list
        except praw.errors.InvalidUser:
            answer = {"code": 400, "message": "Invalid Reddit user, verify your account information", "error" : True}
        except:
            answer = {"code": 400, "message": "General Error", "error" : True}
        finally:
            if answer.get("error", False):
                return Response(answer, status=HTTP_400_BAD_REQUEST)
            return Response(answer)

@api_view(['POST'])
@transaction.atomic
@permission_classes((IsAuthenticated,))
def disaffiliate_subreddits(request):
    answer = {}
    if request.method == 'POST':
        try:
            reddit_user = UserRedditModel.objects.get(user=request.user)
            reddit = praw.Reddit(USER_AGENT, disable_update_check=True)
            reddit.login(reddit_user.user.username, base64.decodestring(reddit_user.reddit_key))
            candidates_list = request.DATA.get('subreddits_list')
            answer_list = []
            if len(candidates_list) > MAX_SUBREDDITS:
                raise Exception("You want to disaffiliate more subreddits of having your account")
            elif reddit_user.subreddits.count() > 0:
                subr_dict = dict()
                for subr_name in candidates_list:
                    subr_name = subr_name.strip().lower()
                    subr_dict['name'] = subr_name
                    try:
                        sub_model = SubRedditModel.objects.get(display_name=subr_name)
                        if (reddit_user.subreddits.filter(id=sub_model.id)):
                            reddit_user.subreddits.remove(sub_model)
                            reddit.get_subreddit(subr_name).unsubscribe()
                            subr_dict['success'] = True
                        else:
                            subr_dict['success'] = False
                            subr_dict['error'] = "Your account is not subscribed to subreddit: %s " % subr_name
                    except ObjectDoesNotExist:
                        subr_dict['success'] = False
                        subr_dict['error'] = "The subreddit %s is not registered in the system" % subr_name
                    answer_list.append(subr_dict)
                answer['subreddits_list'] = answer_list
        except praw.errors.InvalidUser:
            answer = {"code": 400, "message": "Invalid Reddit user, verify your account information", "error" : True}
        except:
            answer = {"code": 400, "message": "General Error", "error" : True}
        finally:
            if answer.get("error", False):
                return Response(answer, status=HTTP_400_BAD_REQUEST)
            return Response(answer)


@api_view(['POST'])
@transaction.atomic
@permission_classes((IsAuthenticated,))
def get_new_posts(request):
    answer = {'success': False}
    if request.method == 'POST':
        try:
            reddit_user = UserRedditModel.objects.get(user=request.user)
            reddit = praw.Reddit(USER_AGENT, disable_update_check=True)
            reddit.login(reddit_user.user.username, base64.decodestring(reddit_user.reddit_key))
            name_reddit = request.DATA.get('name')
            limit_posts = int(request.DATA.get('limit_posts'))
            posts_list = []
            try:
                reddit_user.subreddits.get(display_name=name_reddit)
                submissions = reddit.get_subreddit(name_reddit).get_new(limit=limit_posts)
                answer['success'] = True
                while True:
                    try:
                        sub = next(submissions)
                        post = PostModel()
                        post.id = sub.id
                        post.title = sub.title
                        post.score = sub.ups
                        post.date_created = datetime.datetime.fromtimestamp(int(sub.created_utc)).strftime('%Y-%m-%d %H:%M')
                        post.link = sub.permalink
                        posts_list.append(post.__dict__)
                        answer['success'] = True
                    except:
                        break
                answer['posts_list'] = json.dumps(posts_list)
            except ObjectDoesNotExist:
                answer = {"code": 400, "message": "Your account is not affiliated the subreddit %s" % name_reddit, "error" : True}
        except praw.errors.InvalidUser:
            answer = {"code": 400, "message": "Invalid Reddit user, verify your account information", "error" : True}
        except:
            answer = {"code": 400, "message": "General Error", "error" : True}
        finally:
            if answer.get("error", False):
                return Response(answer, status=HTTP_400_BAD_REQUEST)
            return Response(answer)
